<?php
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
    'appVersion' => '2.2.0',
    'appName' => '<b>Yii</b>Boot',
    'homePage' => 'http://git.oschina.net/penngo/chadmin'
];
